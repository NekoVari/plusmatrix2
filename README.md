# PlusMatrix

บวกเมทริกซ์ 2 เมทริกซ์ มิติ m × n

##ข้อมูลนำเข้า

บรรทัดแรก จำนวนเต็มบวก m, n (1 ≤ m, n ≤ 3) แสดงมิติของเมทริกซ์

บรรทัดที่ 2 ถึงบรรทัดที่ m + 1 จำนวนเต็ม n จำนวนในแต่ละบรรทัด แสดงสมาชิกของเมทริกซ์ที่หนึ่ง บรรทัดที่ m + 2 ถึงบรรทัดที่ 2 · m + 1 จำนวนเต็ม n จำนวนในแต่ละบรรทัด แสดงสมาชิกของเมทริกซ์ที่สอง

##ข้อมูลส่งออก

จำนวนบรรทัดทั้งสิ้น m บรรทัด แต่ละบรรทัดแสดงสมาชิกของเมทริกซ์ผลลัพธ์ ในรูปแบบตามข้อมูลนำเข้า


**ตัวอย่าง**

![Screenshot](Screenshot.png)

